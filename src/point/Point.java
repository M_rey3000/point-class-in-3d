package point;

public class Point {
    private int x, y, z;

    public Point(int x, int y, int z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }
    public double distance(Point p){
        int x = p.x - this.x;
        int y = p.y - this.y;
        int z = p.z - this.z;
        x = x * x;
        y = y * y;
        z = z * z;
        return Math.sqrt(x+y+z);
    }
}

class TestPoint{
    public static void main(String[] args) {
        Point p1 = new Point(8, -5, 0);
        Point p2 = new Point(2, 3, 1);
        System.out.println(p1.distance(p2));
    }
}
